package main

import (
	"fmt"
	"log"
	"net/http"
)

// indexHandler prints the welcome message and 200 ok status code
func indexHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Welcome to my site! Status is: %d", http.StatusOK)
}

func main() {
	http.HandleFunc("/index/", indexHandler)
	log.Fatal(http.ListenAndServe(":9999", nil))
}
